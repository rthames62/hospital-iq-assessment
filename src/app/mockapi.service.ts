import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, retry } from 'rxjs/operators';

export interface Unit {
  name: string;
  id: string;
  capacity: number;
  census: number;
  highAlarm: number;
  lowAlarm: number;
}

@Injectable({
  providedIn: 'root'
})
export class MockapiService {

  constructor(private http: HttpClient) { }

  getUnits(): Observable<Unit[]> {
    return this.http.get('https://private-anon-f1311ccd4b-hospiqtest.apiary-mock.com/units').pipe(
      map((units: Unit[]) => units)
    );
  }
}
