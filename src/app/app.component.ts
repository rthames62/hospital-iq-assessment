import { Component, OnInit } from '@angular/core';
import { MockapiService, Unit } from './mockapi.service';

export interface TableHead {
  name: string;
  id: string;
  sorted: boolean;
  sortedBy: 'asc' | 'desc';
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  units: Unit[];
  tableHeaders: TableHead[] = [
    {
      name: 'Name',
      id: 'name',
      sorted: false,
      sortedBy: 'asc'
    },
    {
      name: 'ID',
      id: 'id',
      sorted: false,
      sortedBy: 'asc'
    },
    {
      name: 'Capacity',
      id: 'capacity',
      sorted: false,
      sortedBy: 'asc'
    },
    {
      name: 'Census',
      id: 'census',
      sorted: false,
      sortedBy: 'asc'
    },
    {
      name: 'Hight Alarm',
      id: 'highAlarm',
      sorted: false,
      sortedBy: 'asc'
    },
    {
      name: 'Low Alarm',
      id: 'lowAlarm',
      sorted: false,
      sortedBy: 'asc'
    }
  ];

  constructor(private mockApi: MockapiService) {}

  ngOnInit() {
    this.mockApi.getUnits().subscribe(units => this.units = units);
  }

  sortBy(header: TableHead) {
    const { id, sortedBy } = header;
    this.tableHeaders.forEach(th => {
      if (th.id === id) {
        th.sortedBy = header.sortedBy === 'asc' ? 'desc' : 'asc';
        th.sorted = true;
      } else {
        th.sorted = false;
        th.sortedBy = 'asc';
      }
    });

    const isAsc = sortedBy === 'asc';
    this.units.sort((a, b) => {
      return this.compare(a[id], b[id], isAsc);
    });
  }

  private compare(a, b, isAsc: boolean) {
    if (typeof a === 'number' || typeof b === 'number') {
      return (a - b) * (isAsc ? -1 : 1);
    }
    return (a < b ? -1 : 1) * (isAsc ? -1 : 1);
  }
}
